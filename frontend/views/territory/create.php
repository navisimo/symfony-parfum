<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Territory */

$this->title = 'Create Territory';
$this->params['breadcrumbs'][] = ['label' => 'Territories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="territory-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
