<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "status".
 *
 * @property int $id
 * @property string $product_id
 * @property int $status
 * @property int $territory_id
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'territory_id'], 'integer'],
            [['product_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'product_id' => '# товара',
            'status' => 'Состояние товара',
            'territory_id' => '# объекта',
        ];
    }

    public function getTerritory()
    {
        return $this->hasOne(Territory::className(), ['id' => 'territory_id']);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
