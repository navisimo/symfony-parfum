<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $item
 * @property string $name
 * @property int $price
 * @property int $purchase_price
 * @property string $img
 * @property string $brand_id
 * @property string $category_id
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'purchase_price'], 'integer'],
            [['item', 'name', 'img', 'brand_id', 'category_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'item' => 'Артикул',
            'name' => 'Наименование',
            'price' => 'Цена',
            'purchase_price' => 'Закупочная цена',
            'img' => 'Изображение',
            'brand_id' => '# бренда',
            'category_id' => '# категории',
        ];
    }

    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
