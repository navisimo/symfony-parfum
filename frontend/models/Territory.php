<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "territory".
 *
 * @property int $id
 * @property string $name
 * @property string $address
 */
class Territory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'territory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'name' => 'Объект',
            'address' => 'Адрес',
        ];
    }
}
