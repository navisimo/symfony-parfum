<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "quantity".
 *
 * @property int $id
 * @property string $product_id
 * @property int $quantity
 * @property int $territory_id
 */
class Quantity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quantity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quantity', 'territory_id'], 'integer'],
            [['product_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'product_id' => '# товара',
            'quantity' => 'в наличие',
            'territory_id' => '# объекта',
        ];
    }

    public function getTerritory()
    {
        return $this->hasOne(Territory::className(), ['id' => 'territory_id']);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

}
