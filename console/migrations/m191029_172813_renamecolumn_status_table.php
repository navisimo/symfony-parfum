<?php

use yii\db\Migration;

/**
 * Class m191029_172813_renamecolumn_status_table
 */
class m191029_172813_renamecolumn_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('status','object_id','territory_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191029_172813_renamecolumn_status_table cannot be reverted.\n";

        $this->renameColumn('status','territory_id','object_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191029_172813_renamecolumn_status_table cannot be reverted.\n";

        return false;
    }
    */
}
