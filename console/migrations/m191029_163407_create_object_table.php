<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%object}}`.
 */
class m191029_163407_create_object_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%object}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'address' => $this->string(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%object}}');
    }
}
