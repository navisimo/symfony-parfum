<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m191029_163431_create_product_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'item' => $this->string(),
            'name' => $this->string(),
            'price' => $this->integer(),
            'purchase_price' => $this->integer(),
            'img' => $this->string(),
            'brand_id' => $this->string(),
            'category_id' => $this->string(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }
}
