<?php

use yii\db\Migration;

/**
 * Class m191029_172833_renamecolumn_quantity_table
 */
class m191029_172833_renamecolumn_quantity_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('quantity','object_id','territory_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191029_172833_renamecolumn_quantity_table cannot be reverted.\n";

        $this->renameColumn('quantity','territory_id','object_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191029_172833_renamecolumn_quantity_table cannot be reverted.\n";

        return false;
    }
    */
}
