<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%quantity}}`.
 */
class m191029_163637_create_quantity_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%quantity}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->string(),
            'quantity' => $this->integer(),
            'object_id' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%quantity}}');
    }
}
