<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%object}}`.
 */
class m191029_171434_drop_object_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%object}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%object}}', [
            'id' => $this->primaryKey(),
        ]);
    }
}
