<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%territory}}`.
 */
class m191029_171623_create_territory_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%territory}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'address' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%territory}}');
    }
}
