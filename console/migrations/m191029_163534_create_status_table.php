<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%status}}`.
 */
class m191029_163534_create_status_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%status}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->string(),
            'status' => $this->integer(),
            'object_id' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%status}}');
    }
}
