<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brand}}`.
 */
class m191029_163327_create_brand_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%brand}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

   public function safeDown()
    {
        $this->dropTable('{{%brand}}');
    }
}
